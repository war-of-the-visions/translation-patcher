import json
import os
from lib import translate, DB , RES, GL_LOC_ID, JP_LOC_ID, ROOT
import UnityPy

DB_PATH = DB
# code

DATABASE = {
    fp[:-5]: json.loads(open(os.path.join(DB_PATH, fp), "rb").read())
    for fp in os.listdir(DB_PATH)
}


def main():
    src = os.path.join(RES, JP_LOC_ID)
    e = UnityPy.load(src)
    for cont, obj in e.container.items():
        data = obj.read()
        translated = patch_file(data.name, data.script)
        data.script = json.dumps(translated, ensure_ascii=False, indent=4).encode(
            "utf8"
        )
        data.save()
        
    with open(os.path.join(ROOT, JP_LOC_ID), "wb") as f:
        f.write(e.file.save())


def patch_file(name, script):
    print(name)

    # ensure that it's a useable file
    script = json.loads(bytes(script))

    if "infos" not in script or not script["infos"]:
        return

    # load the suiting translation database
    dkey = name.lower()
    if dkey not in DATABASE:
        DATABASE[dkey] = {}
    db = DATABASE[dkey]

    # patch the entries
    infos = []
    change = False
    for item in script["infos"]:
        key, value = item.values()
        if key not in db:
            print(key)
            # if key not found in db translate it
            db[key] = {"ja": value, "en": translate(value)}
            change = True
        infos.append({"key": key, "value": db[key]["en"]})

    # update the script
    script["infos"] = infos

    # update db change
    if change:
        with open(os.path.join(DB_PATH, name.lower() + ".json"), "wb") as f:
            f.write(json.dumps(db, ensure_ascii=False, indent="\t").encode("utf8"))

    return script


if __name__ == "__main__":
    main()
