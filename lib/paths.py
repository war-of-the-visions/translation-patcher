import os

ROOT = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
RES = os.path.join(ROOT, "res")
DB = os.path.join(RES, "database")