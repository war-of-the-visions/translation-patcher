import json
import os
import re
import time
#from googletrans import Translator
from pygoogletranslation import Translator

from .paths import RES

reJP = re.compile(
    r"([\u3041-\u3096\u30A0-\u30FF\u3400-\u4DB5\u4E00-\u9FCB\uF900-\uFA6A\u2E80-\u2FD5\uFF5F-\uFF9F\u3000-\u303F\u31F0-\u31FF\u3220-\u3243\u3280-\u337F\uFF01-\uFF5E]+)"
)
reEn = re.compile(r'[a-zA-Z0-9 -?!á-ú"§$%&/()\=\+\*\']+')


def ret_k_v(dic, ret={}):
    for key, value in dic.items():
        if isinstance(value, dict):
            ret_k_v(value, ret)
        elif isinstance(value, str):
            if reJP.match(key) and value not in ret and reEn.match(value):
                ret[key] = value
    return ret


with open(os.path.join(RES, "FF_Wiki_Translations.json"), "rb") as f:
    FF = ret_k_v(json.loads(f.read()))
    FF["？？？"]

TAC = {}
with open(os.path.join(RES, "TAC_Translations.json"), "rb") as f:
    data = json.loads(f.read())
    for mdata in data.values():
        for data in mdata.values():
            for item in data.values():
                if (
                    "kanji" in item
                    and item["kanji"]
                    and "official" in item
                    and item["official"]
                ):
                    TAC[item["kanji"]] = item["official"]


def translate(key):
    # if new key add key to existing table after translation
    if key in FF:
        ret = FF[key]
        if reEn.match(ret):
            return ret

    if key in TAC:
        ret = TAC[key]
        if reEn.match(ret):
            return ret

    return fix_css(translate_google(key).strip(" "))


reCSS = re.compile(r"<(.+?)>")


def fix_css(text):
    t = reCSS.findall(text)
    if not t:
        return text
    for item in set(t):
        text = text.replace(item, item.lower().replace(" ", ""))
    return text


def translate_google(text):
    translator = Translator()#Translator(service_urls=['translate.googleapis.com'])
    if not text.strip(" "):
        return text
    # time.sleep(1)
    try:
        return translator.translate(text, dest="en", src="ja").text
    except json.JSONDecodeError:
        input("DecodeError")
        return translate(text)
    except (ConnectionError, AttributeError) as e:
        print(e)
        time.sleep(1)
        return translate(text)