from .paths import *
from .api import API
from .version import update_version
from .downloader import download_loc_asset
from .translation import translate

GL_LOC_ID = "313509430"
JP_LOC_ID = "522608825"