import os
import json
from lib import API, download_loc_asset, update_version, RES, GL_LOC_ID, JP_LOC_ID

APK = {
	"global" : "com.square_enix.android_googleplay.WOTVffbeww",
	"japan"  : "com.square_enix.WOTVffbejp"
}

for ver, loc_id, lang in [
	("global", GL_LOC_ID, "en"),
	("japan", JP_LOC_ID, "ja")
	]:
	extract = os.path.join(RES, ver)

	os.makedirs(RES, exist_ok=True)

	try:
		with open(os.path.join(RES, f"version_{ver}.json"), "rt", encoding="utf8") as f:
			HOST, VERSION = list(json.load(f).values())

		environment = API(api = HOST.split('://',1)[1][:-1]).pass_enviroment(VERSION)
	except (KeyError, FileNotFoundError):
		HOST, VERSION = update_version(APK[ver])
		# save new host and version
		with open(os.path.join(RES, f"version_{ver}.json"), "wt", encoding="utf8") as f:
			json.dump({"host":HOST, "version":VERSION}, f, ensure_ascii=False, indent="\t")

		environment = API(api = HOST.split('://',1)[1][:-1]).pass_enviroment(VERSION)

	with open(os.path.join(RES, loc_id), "wb") as f:
		f.write(download_loc_asset(environment, f"re/{loc_id}", lang))

# update DB with GL data
from lib import DB
import UnityPy
src = os.path.join(RES, GL_LOC_ID)
e = UnityPy.load(src)
for cont, obj in e.container.items():
	try:
		data = obj.read()
		dbf = os.path.join(DB, f"{data.name.lower()}.json")
		if not os.path.exists(dbf):
			continue

		infos = {
			item["key"] : item["value"]	
			for item in json.loads(bytes(data.script))["infos"]
		}

		
		with open(dbf, "rb") as f:
			db = json.loads(f.read())
		
		update = False
		for key, value in infos.items():
			if value and key in dbf and dbf[key]["en"] != value:
				update = True
				db[key]["en"] = value

		if update:
			with open(dbf, "wb") as f:
				f.write(json.dumps(db, ensure_ascii=False, indent=4).encode("utf8"))
			print(f"Updated {data.name} translation")
	except:
		pass